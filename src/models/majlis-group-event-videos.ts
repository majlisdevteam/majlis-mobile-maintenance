import { GroupEventModel } from "./group-event";

export class MajlisGroupEventVideos {
    videoId = null;
    videoFilePath =null;
    userInserted = 0;
    dateInserted = null;
    eventId:GroupEventModel = null;
}