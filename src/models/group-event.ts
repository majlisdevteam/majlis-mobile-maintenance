

import { MajlisGroupModel } from "./majlis-group";
import { MajlisMdCode } from "./majlis-md-code";
import { EventCategoryModel } from "./event-category";
import { NotificationModel } from "./majlis-notification";
import { MajlisGroupEventVideos } from "./majlis-group-event-videos";
import { MajlisEventComment } from "./majlis-event-comment";
import { MajlisGroupEventImage } from "./majlis-group-event-image";

export class GroupEventModel {
    eventId = null;
    eventSystemId = null;
    eventCaption = null;
    eventIconPath = null;
    eventMessage = null;
    eventLocation = null;
    eventLocationLat = null;
    eventLocationLont = null;
    eventTime = null;
    userInserted = null;
    dateInserted = null;
    userModified = null;
    dateModified = null;
    majlisGroupEventStatusList = [];
    groupId: MajlisGroupModel = null;
    eventStatus: MajlisMdCode = null;
    eventCategory: EventCategoryModel = new EventCategoryModel();
    // majlisNotificationCollection: Array<NotificationModel> = null;
    // majlisGroupEventVideosCollection: Array<MajlisGroupEventVideos> = null;
    // majlisEventCommentCollection: Array<MajlisEventComment> = null;
    // majlisGroupEventImagesCollection: Array<MajlisGroupEventImage> = null;
    manualCount = null;
    evtCnt = null;
    attendStatus : null; 


}