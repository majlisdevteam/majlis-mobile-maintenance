import { NotificationModel } from "./majlis-notification";
import { MajlisMdCode } from "./majlis-md-code";
import { MajlisGroupModel } from "./majlis-group";

export class MajlisCMSUser {
    userId = null;
    userFullName = null;
    expiryDate = null;
    noAllowedNotifications = 0;
    userInserted = null;
    dateInserted = null;
    userModified = null;
    dateModified = null;
    majlisNotificationCollection :Array<NotificationModel> = null;
    status: MajlisMdCode = null;
    majlisGroupCollection:Array<MajlisGroupModel> = null;

}