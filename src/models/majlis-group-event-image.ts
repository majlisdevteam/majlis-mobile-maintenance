import { GroupEventModel } from "./group-event";

export class MajlisGroupEventImage {

    eventImageId = null;
    eventImagePath = null;
    userInserted = 0;
    dateInserted = null;
    eventId: GroupEventModel = null;
    
}