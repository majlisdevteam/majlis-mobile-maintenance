
import { ProvinceDetailsModel } from "./province-details";

export class ProvinceModel {

countryCode : string;
provinces : Array<ProvinceDetailsModel> = new Array<ProvinceDetailsModel>();

}