import { MajlisEventCommentReply } from "./majlis-event-commentreply";
import { GroupEventModel } from "./group-event";

export class MajlisEventComment {

    commentId = null;
    commentMessage = null;
    dateInserted = null;
    userInserted = 0;
    majlisEventCommentReplyList:Array<MajlisEventCommentReply> = null;
    commentEventId: GroupEventModel = null;
}