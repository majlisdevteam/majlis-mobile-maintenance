
import { GroupEventModel } from "./group-event";
import { NotificationModel } from "./majlis-notification";
import { MajlisMdCode } from "./majlis-md-code";
import { MajlisCMSUser } from "./cms-user";

export class MajlisGroupModel {

    groupId = null;
    groupSystemId = null;
    groupTitle = null;
    groupIconPath = null;
    groupDescription = null;
    groupCountry = null;
    groupDistrict = null;
    groupCity = null;
    userInserted = null;
    dateInserted = null;
    userModified = null;
    expiryDate = null;
    dateModified = null;
    majlisMobileUsersList = null;
    majlisGroupEventList = null;
    majlisNotificationList = null;
    groupPublicLevel = null;
    groupAdmin = null;
    groupStatus = null;




}
