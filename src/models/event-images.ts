import { NotificationModel } from "./majlis-notification";
import { MajlisMdCode } from "./majlis-md-code";
import { MajlisGroupModel } from "./majlis-group";

export class EventImages {

    public  imageData = new Array<string>();

    public imageEvent = null;

    public imageOwner : number = null;

}