
import { GroupEventModel } from "./group-event";
import { MajlisMdCode } from "./majlis-md-code";
import { MobileUserModel } from "./mobile-users";

export class EventCategoryModel {
    categoryId = null;
    categorySystemId = null;
    categoryTitle = null;
    categoryIconPath = null;
    categoryDescription = null;
    userInserted = null;
    dateInserted = null;
    userModified = null;
    dateModified = null;
    categoryStatus = null;

}