
import { MobileUsersEventCategoryModel } from "./mobileusers-event-category";
import { EventCategoryModel } from "./event-category";
import { MajlisGroupModel } from "./majlis-group";

export class MobileUserModel {

    mobileUserId = null;
    mobileUserMobileNo = null;
    mobileUserName = null;
    mobileUserEmail = null;
    mobileUserDob = null;
    mobileUserCountry = "AE";
    mobileUserProvince = null;
    mobileUserCity = null;
    mobileUserDateRegistration = null;
    mobileUserDateModified = null;
    mobileUserPassCode = null;
    majlisCategoryList : Array<EventCategoryModel> = [];
    majlisGroupList : Array<MajlisGroupModel> = [];
}