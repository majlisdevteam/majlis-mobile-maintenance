import { MajlisEventComment } from "./majlis-event-comment";

export class MajlisEventCommentReply {
    
    commentReplyId = null;
    replyCommentMessage = null;
    dateInserted = null;
    userInserted = 0;
    parentCommentId:Array<MajlisEventComment> = null;
}