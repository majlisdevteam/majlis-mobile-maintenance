import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';
import { AppParams } from "../app/app.module";
import { MobileUserModel } from "../models/mobile-users";

/*
  Generated class for the MobileUserService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class NotificationService {

  constructor(public http: Http) {
    console.log('Hello EventDetailsService Provider');
  }

  public getNotification(userId: number) {
    let url = AppParams.BASE_PATH + "notification/getNotificationList?userid=" + userId;

    return this.http.get(url).map(response => response.json());
  }


  public getNotificationDetails(notifId: number) {
    let url = AppParams.BASE_PATH + "notification/getNotificationList?notificationId=" + notifId;

    return this.http.get(url).map(response => response.json());
  }

  public getEventNotification(eventId: number) {
    let url = AppParams.BASE_PATH + "notification/getNotificationList?eventid=" + eventId;

    return this.http.get(url).map(response => response.json());
  }



} 
