import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';
import { AppParams } from "../app/app.module";
import { MobileUserModel } from "../models/mobile-users";

/*
  Generated class for the MobileUserService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MobileUserService {

  constructor(public http: Http) {
    console.log('Hello MobileUserService Provider');
  }

  public saveMobileUsers(body: MobileUserModel) {


    let url = AppParams.BASE_PATH + "users/createMobileUser";

    return this.http.post(url, body).map(response => response.json());


  }

  public requestVerification(mobileNo) {

    let url = AppParams.BASE_PATH + "users/getVerification?userMobileNumber="+mobileNo;
    return this.http.get(url).map(response => response.json());
  }

    public requestFinalVerification(mobileNo,code) {

    let url = AppParams.BASE_PATH + "users/finalVerification?userMobileNumber="+mobileNo+"&userVerification="+code;
    return this.http.get(url).map(response => response.json());
  }

  public updateMobileUsers(body: MobileUserModel) {


    let url = AppParams.BASE_PATH + "user/modifyMobileUser";

    return this.http.post(url, body).map(response => response.json());


  }

  public getMobileUsers(id: number) {


    let url = AppParams.BASE_PATH + "user/getMobileUserById?userId=" + id;

    return this.http.get(url).map(response => response.json());


  }

  public updateMobileUsersById(id: number) {





  }


} 
