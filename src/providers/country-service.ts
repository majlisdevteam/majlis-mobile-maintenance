import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the CountryService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CountryService {

  constructor(public http: Http) {
    console.log('Hello CountryService Provider');
  }

  public getCountryList(){
    return this.http.get("assets/countries.json").map(response=> response.json());
  }

   public geProvinceList(){
    return this.http.get("assets/provinces.json").map(response=> response.json());
  }

      public extractData(res: Response) {
    let body = res.json();
    return body || { };
  }
} 
