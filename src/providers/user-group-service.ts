import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';
import { AppParams } from "../app/app.module";
import { MobileUserModel } from "../models/mobile-users";

/*
  Generated class for the MobileUserService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserGroupService {

  constructor(public http: Http) {
    console.log('Hello UserGroupService Provider');
  }

  public getUserGroup(start: number, groupCountry, province,city) {

    let end = AppParams.LIMIT;

    let url = AppParams.BASE_PATH + "groups/getAllMobileGroups?groupCountry="+groupCountry+"&groupDistrict="+province+"&groupCity="+city;

    return this.http.get(url).map(response => response.json());


  }

  public getEventsByGroup(groupId){
    let url =  AppParams.BASE_PATH + "groups/getEventsByGroupMobile?groupId="+groupId;

    return this.http.get(url).map(response=>response.json());

  }

} 
