import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';
import { AppParams } from "../app/app.module";
import { MajlisEventComment } from "../models/majlis-event-comment";
import { RSVPStatusModel } from "../models/rsvp-status";
import { EventFeedbackModel } from "../models/event-feedback";

/*
  Generated class for the MobileUserService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class EventDetailsService {

  constructor(public http: Http) {
    console.log('Hello EventDetailsService Provider');
  }

  
  public getEventDetailsSearch(search) {


    let url = AppParams.BASE_PATH + "events/getUserEvents?search="+search;

    return this.http.get(url).map(response => response.json());


  }

  public getEventDetails(eventId) {


    let url = AppParams.BASE_PATH + "events/getUserEvents?eventId=" + eventId;

    return this.http.get(url).map(response => response.json());


  }

  public getEventWall(userId,start,limit) {

    let url = AppParams.BASE_PATH + "events/getUserEvents?userId=" + userId+"&start="+start+"&limit="+limit;

    return this.http.get(url).map(response => response.json());

  }

  public getEventStatus() {

    let url = AppParams.BASE_PATH + "event/getEventStatus?eventId=20";

    return this.http.get(url).map(response => response.json());

  }

  public getEventStatusByEvent(eventId) {

    let url = AppParams.BASE_PATH + "event/getEventStatus?eventId=" + eventId;

    return this.http.get(url).map(response => response.json());

  }


  public saveEventStatus(options: RSVPStatusModel) {
    let url = AppParams.BASE_PATH + "event/updateRsvpStatus";

    return this.http.post(url, options).map(response => response.json())
  }

  public saveEventImages(body) {

    let url = AppParams.BASE_PATH + "event/saveEventImage";

    return this.http.post(url, body).map(response => response.json());

  }


  public getEventComment(eventId) {

    let url = AppParams.BASE_PATH + "event/getComments?eventId=" + eventId;

    return this.http.get(url).map(response => response.json());

  }

  public saveEventComment(body: MajlisEventComment) {

    let url = AppParams.BASE_PATH + "event/postComments";

    return this.http.post(url, body).map(response => response.json());

  }

  public getEventImage(eventId) {

    let url = AppParams.BASE_PATH + "event/getEventImages?eventId=" + eventId;

    return this.http.get(url).map(response => response.json());

  }




  public getEventVideo(eventId) {

    let url = AppParams.BASE_PATH + "event/getEventVideos?eventId=" + eventId;

    return this.http.get(url).map(response => response.json());

  }


  public getEventResponse(eventId) {

    let url = AppParams.BASE_PATH + "events/getEventList?eventId=" + eventId;

    return this.http.get(url).map(response => response.json());

  }


  public getEventRSVPStatus(eventId, userId) {

    let url = AppParams.BASE_PATH + "events/getEventStatusUser?userId=" + userId + "&eventId=26";

    return this.http.get(url).map(response => response.json());

  }

public saveEventFeedback(body: EventFeedbackModel) {

    let url = AppParams.BASE_PATH + "events/updateEventFeedback";

     return this.http.post(url, body).map(response => response.json());

  }



} 
