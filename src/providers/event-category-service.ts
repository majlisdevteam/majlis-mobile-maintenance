import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';
import { AppParams } from "../app/app.module";
import { MobileUserModel } from "../models/mobile-users";

/*
  Generated class for the MobileUserService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class EventCategoryService {

  constructor(public http: Http) {
    console.log('Hello MobileUserService Provider');
  }

  public getAllCategory() {


    let url = AppParams.BASE_PATH + "category/getCategoryDetailsMobile";

    return this.http.get(url).map(response => response.json());


  }

} 
