import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, ToastController, Toast, NavController, LoadingController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network'
import { Storage } from '@ionic/storage';


import { Start } from "../pages/start/start";
import { Notifications } from "../pages/notifications/notifications";
import { HistoryPage } from "../pages/history-page/history-page";
import { FindEventsPage } from "../pages/find-events/find-events";
import { ProfilePage } from "../pages/profile-page/profile-page";
import { AboutUs } from "../pages/about-us/about-us";
import { EventWall } from "../pages/event-wall/event-wall";

@Component({
  templateUrl: 'app.html',
  providers: [Network]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;






  public userId: any;

  rootPage: any;
  static toast: Toast;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private toastCtrl: ToastController, private network: Network, public storage: Storage,
    private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
    platform.ready().then(() => {
      this.rootPage = Start;

      // // ckeck if user completed registration process
      // this.storage.get('mobile_user_id').then(mobileUserId => {
      //   // console.log("MOBILE_USER_ID>>>>>>>>>>", mobileUserId);

      //   if (mobileUserId != undefined) {
      //     this.nav.setRoot(EventWall);
      //   } else {
      //     this.rootPage = Start;
      //   }
      // });

      this.network.onDisconnect().subscribe(() => {
        console.log("disconnect")
        MyApp.toast = toastCtrl.create({
          message: 'No internet connection '
        });
        MyApp.toast.present();
      });

      this.network.onConnect().subscribe(() => {
        console.log("connect")
        MyApp.toast.dismissAll();
      });

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });


    this.storage.get('mobile_user_id').then(mobileUserId => {
      console.log("MOBILE_USER_ID>>>>>>>>>>", mobileUserId);
      this.userId = mobileUserId;
    });

  }

  openNearbyEventsPage() {
    this.nav.setRoot(EventWall);
  }

  openNotificationsPage() {
    this.nav.push(Notifications);
  }

  openHistoryPage() {
    this.nav.push(HistoryPage);
  }

  openFindEventsPage() {
    this.nav.push(FindEventsPage);
  }

  openProfilePage() {
    this.nav.push(ProfilePage);
  }

  openAboutUsPage() {
    this.nav.push(AboutUs);
  }

  notRegisteredAlert() {
    let alert = this.alertCtrl.create({
      title: 'Warning',
      message: 'Please Register!',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
            this.nav.root(Start);
          }
        }
      ]
    });
    alert.present();
  }
}