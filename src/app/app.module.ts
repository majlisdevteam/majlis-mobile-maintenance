import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { Calendar } from '@ionic-native/calendar';
import { MediaCapture } from "@ionic-native/media-capture";
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Start } from "../pages/start/start";
import { TermsPage } from "../pages/terms/terms";
import { DisclaimerPage } from "../pages/disclaimer/disclaimer";
import { RegistrationPage } from "../pages/registration/registration";
import { EventWall } from "../pages/event-wall/event-wall";
import { CountryList } from "../pages/country-list/country-list";
import { HttpModule } from "@angular/http";
import { NearbyEvents } from "../pages/nearby-events/nearby-events";
import { Notifications } from "../pages/notifications/notifications";
import { HistoryPage } from "../pages/history-page/history-page";
import { FindEventsPage } from "../pages/find-events/find-events";
import { ProfilePage } from "../pages/profile-page/profile-page";
import { AboutUs } from "../pages/about-us/about-us";
import { EventComments } from "../pages/event-comments/event-comments";
import { ExpandNotification } from "../pages/expand-notification/expand-notification";
import { AddImagesVideos } from "../pages/add-images-videos/add-images-videos";
import { Feedback } from "../pages/feedback/feedback";
import { GroupSelect } from "../pages/group-select/group-select";
import { EventNotifications } from "../pages/event-notifications/event-notifications";
import { ExpandEventVideo } from "../pages/expand-event-image/expand-event-video";
import { ExpandEventImage } from "../pages/expand-event-video/expand-event-image";
import { ProvinceList } from "../pages/province-list/province-list";
import { CityList } from "../pages/city-list/city-list";
import { MobileNumber } from "../pages/mobile-number/mobile-number";
import { GroupDetails } from "../pages/group-details/group-details";
import { ImgFallbackModule } from 'ngx-img-fallback';
@NgModule({
  declarations: [
    MyApp,
    Start,
    TermsPage,
    DisclaimerPage,
    RegistrationPage,
    EventWall,
    CountryList,
    NearbyEvents,
    Notifications,
    HistoryPage,
    FindEventsPage,
    ProfilePage,
    AboutUs,
    MobileNumber,
    EventComments,
    ExpandNotification,
    AddImagesVideos,
    Feedback,
    GroupSelect,
    ProvinceList,
    CityList,
    EventNotifications,
    ExpandEventVideo,
    ExpandEventImage,
    GroupDetails
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ImgFallbackModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__tmpDb',
      driverOrder: ['sqlite', 'indexeddb', 'websql']
    }),
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Start,
    TermsPage,
    DisclaimerPage,
    RegistrationPage,
    EventWall,
    CountryList,
    NearbyEvents,
    Notifications,
    HistoryPage,
    FindEventsPage,
    MobileNumber,
    ProfilePage,
    ProvinceList,
    CityList,
    AboutUs,
    EventComments,
    ExpandNotification,
    AddImagesVideos,
    Feedback,
    GroupSelect,
    EventNotifications,
    ExpandEventVideo,
    ExpandEventImage,
    GroupDetails
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Camera,
    Calendar,
    MediaCapture,
    File,
    Transfer,
    FilePath,
    SQLite
  ]
})
export class AppModule {
  
 }


export const AppParams = Object.freeze({
  BASE_PATH : "http://35.154.0.116:8080/APIGateway-1.0/",
//  BASE_PATH: "http://192.0.0.222:8080/APIGateway-1.0/",
  // VIDEO_SERVER_PATH: "http://192.0.0.222:8080/Majlis-EventManagement_merge-1.0/service/",
  VIDEO_SERVER_PATH : "http://35.154.0.116:8080/Majlis-EventManagement_merge-1.0/service/",
  ABOUT: 'Test about us ',
  START: 0,
  LIMIT: 300
})