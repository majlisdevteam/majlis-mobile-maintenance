import { Component, ViewChild, EventEmitter, Output } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, FabContainer, Select, AlertOptions, PopoverController, Header, ToastController } from 'ionic-angular';
import { EventComments } from "../event-comments/event-comments";
import { AddImagesVideos } from "../add-images-videos/add-images-videos";
import { Feedback } from "../feedback/feedback";
import { Calendar } from '@ionic-native/calendar';
import { EventDetailsService } from "../../providers/event-details-service";
import { GroupEventModel } from "../../models/group-event";
import { FormGroup } from "@angular/forms";
import { EventNotifications } from "../event-notifications/event-notifications";
import { AlertInputOptions } from "ionic-angular/components/alert/alert-options";
import { ExpandEventVideo } from "../expand-event-image/expand-event-video";
import { ExpandEventImage } from "../expand-event-video/expand-event-image";
import { Storage } from '@ionic/storage';
import { RSVPStatusModel } from "../../models/rsvp-status";
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";


/**
 * Generated class for the NearbyEvents page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-nearby-events',
  templateUrl: 'nearby-events.html',
  providers: [EventDetailsService, GroupEventModel, RSVPStatusModel]
})
export class NearbyEvents {
  eventWall: GroupEventModel[];



  private eventDetails: GroupEventModel = new GroupEventModel();
  private eventDetailsStatus: Array<GroupEventModel> = [];
  private eventDetailsStatusTemp: Array<GroupEventModel> = [];

  private eventDetailImages: Array<GroupEventModel> = [];
  private eventDetailVideo: Array<GroupEventModel> = [];

  private eventResponseCount: Array<GroupEventModel> = [];
  private eventResponseCountTemp: Array<GroupEventModel> = [];

  public manualResponseCount;
  public realResponseCount;


  private eventLongitude;
  private eventLatitude;


  public eventTitleCal;
  public eventLocCal;
  public eventMsgCal;
  public eventDateCal;

  public eventDateTime;

  public eventRsvpStatus;


  eventId: number = 0;



  eventStatusForm: FormGroup;
  status = null;

  //public startDate:Date;
  public startDate = new Date("May 18, 2017 10:13:00");
  public endDate = new Date("May 19, 2017 10:13:00");
  public abc: any;

  public userId: any;


  fabButtonOpened: Boolean;
  eventTitle: String = "Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night Live Music Night";
  eventImages = ['1.jpg', '2.jpg'];


  @ViewChild('map') mapElement;
  map: any;

  @Output() valueChange = new EventEmitter();

  @ViewChild('selecteRSVP') selecteRSVP: Select;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController,
    public calendar: Calendar, private loadingCtrl: LoadingController,
    public getEventDetails: EventDetailsService, public groupEvent: GroupEventModel,
    public popoverCtrl: PopoverController, public storage: Storage, private sqlite: SQLite, public toastCtrl: ToastController) {
    this.fabButtonOpened = false;
  }



  eventStatus(eventId) {
    console.log(">>>> ", eventId)

    this.eventId = eventId;
    this.selecteRSVP.open();
  }



  createEvent(fab: FabContainer, eventId) {

    let value = localStorage.getItem(eventId);


    if (value == null) {

      fab.close();
      let timestamp = new Date(this.eventDateCal);
      this.calendar.createEvent(this.eventTitleCal, this.eventLocCal, this.eventMsgCal, timestamp, timestamp);
      let alert = this.alertCtrl.create({
        title: 'New Event!',
        message: 'Event successfully added to calendar.',
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: () => {
              localStorage.setItem(eventId, "true");
            }
          }
        ]
      });
      alert.present();

    } else {

      this.toastCtrl.create({
        message: "You cannot add this event again",
        duration: 3000
      }).present();

      fab.close();

    }

  }

  selectRsvp(value) {
    console.log("rsvp..... ", value);

    let val = "";

    let idx = this.eventWall.findIndex(e => e.eventId == this.eventId);

    if (value == 1) {

      this.eventWall[idx].majlisGroupEventStatusList[0].status = "Attending";

      this.eventWall[idx].majlisGroupEventStatusList[0].statusId = 1;


      val = "Attending";
    } else if (value == 2) {

      this.eventWall[idx].majlisGroupEventStatusList[0].status = "Not Attending";

      this.eventWall[idx].majlisGroupEventStatusList[0].statusId = 2;

      val = "Not Attending";
    } else {


      this.eventWall[idx].majlisGroupEventStatusList[0].status = "Pending";

      this.eventWall[idx].majlisGroupEventStatusList[0].statusId = 3;

      val = "Pending";

    }

    this.eventRsvpStatus = val;

    let rsvpStatus: RSVPStatusModel = new RSVPStatusModel();

    rsvpStatus.eventId = this.eventId
    rsvpStatus.statusId = value;
    rsvpStatus.userId = this.userId;

    this.getEventDetails.saveEventStatus(rsvpStatus).subscribe(res => {
      console.log("res ", res);
      this.saveMobileUserHistory();

    }, error => {

    })

  }

  // save event RSVP to local database
  // database connection
  public databseConnection() {
    return this.sqlite.create({
      name: "MajlisDB.db",
      location: "default"
    });

  }

  //save data into database
  saveMobileUserHistory() {
    console.log("Save Images clicked...................................");
    this.databseConnection().then((database: SQLiteObject) => {


      database.executeSql("INSERT INTO tbl_MajlisHistory (History_Type, History_Message) VALUES (?,?)", ['Event', 'You have added RSVP']).then((data) => {
        console.log("INSERTED COMMENT TO DATABASE SUCCESSFULLY .................. ", data);
      }, (error) => {
        console.log("ERROR INSERTION COMMENT TO DATABASE ..................", JSON.stringify(error.err));
      });

    });
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad NearbyEvents');


    this.eventTitleCal = this.navParams.get('EventTitle');
    this.eventLocCal = this.navParams.get('EventLoc');
    this.eventMsgCal = this.navParams.get('EventMsg');
    this.eventDateCal = this.navParams.get('EventDate');
    this.eventDateTime = this.navParams.get('EventTime');

    this.eventId = this.navParams.get('EventID');

    this.eventWall = new Array<GroupEventModel>()

    this.eventWall = this.navParams.get("eventObj");

    this.eventRsvpStatus = this.navParams.get('RsvpStatus');

    let loader = this.loadingCtrl.create({
      content: "Loading",
    });
    loader.present();

    this.storage.get('mobile_user_id').then(mobileUserId => {
      this.userId = mobileUserId;
    });


    this.getEventDetails.getEventDetails(this.eventId).subscribe(response => {

      let res = response.getUserEvents;
      if (res.responseCode == 1) {


        this.eventDetails = res.responseData[0];

        this.eventLongitude = res.responseData[0].eventLocationLat;
        this.eventLatitude = res.responseData[0].eventLocationLont;

        this.initMap(this.eventLongitude, this.eventLatitude);


      } else {
        this.responsAlert();
      }
      loader.dismiss();



    }, error => {
      loader.dismiss();
      this.responsAlert();

    });



    // Get event status
    this.getEventDetails.getEventStatus().subscribe(response => {

      let res = response.getEventStatus;
      if (res.responseCode == 1) {

        this.eventDetailsStatus = res.responseData;

        this.eventDetailsStatusTemp.forEach(value => {

          this.eventDetailsStatus.push(value);

        });



      }


    }, error => {

    });




    //Get Event Images
    this.getEventDetails.getEventImage(this.eventId).subscribe(response => {
      console.log("EVENT_IMAGES>>>>>", response);
      let res = response.getEventImages;
      if (res.responseCode == 1) {

        console.log("EVENT_IMAGE_RESPONSE_DATA", res.responseData);

        this.eventDetailImages = res.responseData;
        console.log("EVENT_IMAGES>>>>>>>>>>", this.eventDetailImages);


      }


    }, error => {

    });



    //Get Event Video
    this.getEventDetails.getEventVideo(this.eventId).subscribe(response => {
      console.log("EVENT_VIDEO>>>>>", response);
      let res = response.getEventVideos;
      if (res.responseCode == 1) {

        console.log("EVENT_VIDEO_RESPONSE_DATA", res.responseData);

        this.eventDetailVideo = res.responseData;
        console.log("EVENT_VIDEO>>>>>>>>>>", this.eventDetailVideo);


      }


    }, error => {

    });


    // Get Event Response count
    this.getEventDetails.getEventResponse(this.eventId).subscribe(response => {
      // console.log("EVENT_RESPONSE COUNT>>>>>", response);
      let res = response.getEventDetails;


      // console.log("EVENT_IMAGE_RESPONSE_DATA", res.responseData);

      this.eventResponseCount = res.responseData;

      // let arr=[];
      //       this.eventResponseCount

      this.eventResponseCount.forEach(value => {

        this.eventResponseCountTemp.push(value);
        console.log("value", value);
        this.manualResponseCount = value.manualCount;
        this.realResponseCount = value.evtCnt;


      })




    }, error => {

    });


  }

  initMap(long, latit) {
    let latLng = new google.maps.LatLng(long, latit);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      draggable: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(long, latit);
  }

  addMarker(long, lat) {

    let latLng = new google.maps.LatLng(long, lat);
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      icon: "assets/img/mapmarker.png",
    });

    //let content = "<h4>Information!</h4>";  
    //let content = des;

    this.addInfoWindow(marker, this.eventTitle);

  }

  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }


  eventStatusAlert() {
    let alert = this.alertCtrl.create({
      title: 'Status',

      inputs: [
        {
          type: 'radio',
          label: 'Not Attending',
          value: 'notattend',
          checked: true
        },
        {
          type: 'radio',
          label: 'Attending',
          value: 'attend'
        },
        {
          type: 'radio',
          label: 'Maybe',
          value: 'maybe'
        }


      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Ok',
          handler: (data: any) => {
            console.log('Radio data:', data);
          }
        }
      ]
    });

    alert.onDidDismiss(data => {
      console.log("data is ", data)
    });

    alert.present();
  }

  openEventNotification(eventId: number) {

    let loader = this.loadingCtrl.create({
      content: "Loading",
    });
    loader.present();
    this.navCtrl.push(EventNotifications, {
      EventID: eventId
    });
    loader.dismiss();

  }

  openEventCommentsPage(fab: FabContainer, eventId: number, eventTitle: string) {
    fab.close();
    this.navCtrl.push(EventComments, {
      EventID: eventId,
      EventTitle: eventTitle
    });
  }
  openImageVideo(fab: FabContainer, eventId: number) {
    fab.close();
    this.navCtrl.push(AddImagesVideos, {
      EventID: eventId
    });
  }
  openFeedback(fab: FabContainer) {
    fab.close();
    this.navCtrl.push(Feedback);
  }
  openFabButton() {
    if (this.fabButtonOpened == false) {
      this.fabButtonOpened = true;
    } else {
      this.fabButtonOpened = false;
    }
  }


  responsAlert() {
    let alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Cannot connect to the server',
      buttons: [
        {
          text: 'Try Again',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
            this.ionViewDidLoad();
          }
        }
      ]
    });
    alert.present();
  }



  rsvpSendData() {
    console.log("RADIO CLICKED");
  }



  expandImage(eventImageId) {
    this.navCtrl.push(ExpandEventImage, {
      EventImageID: eventImageId
    });
  }



  expandVideo(eventVideoId) {
    this.navCtrl.push(ExpandEventVideo, {
      EventVideoID: eventVideoId
    });
  }

}
