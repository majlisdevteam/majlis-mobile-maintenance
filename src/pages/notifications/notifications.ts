import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ExpandNotification } from "../expand-notification/expand-notification";
import { NotificationService } from "../../providers/notification-service";
import { NotificationModel } from "../../models/majlis-notification";
import { Storage } from '@ionic/storage';

/**
 * Generated class for the Notifications page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
  providers: [NotificationService]
})
export class Notifications {



  public notificationList = new Array<NotificationModel>();
  public notificationListTemp = new Array<NotificationModel>();





  constructor(public navCtrl: NavController, public navParams: NavParams,
    public getNotifications: NotificationService, public storage: Storage,
    private alertCtrl: AlertController) {



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Notifications');


    this.storage.get('mobile_user_id').then(mobileUserId => {
      console.log("MOBILE_USER_ID>>>>>>>>>>", mobileUserId);

      if (mobileUserId != undefined) {



        //Get Notifications
        this.getNotifications.getNotification(mobileUserId).subscribe(response => {
          console.log("NOTIFICATIONS>>>>>>>>>", response);
          let res = response.notificationList;
          if (res.responseCode == 1) {

            console.log("NOTIFICATIONS_RESPONSE>>>>>>>>>>", res.responseData);

            this.notificationList = res.responseData;
            console.log("NOTIFICATIONS_RESPONSE_DATA>>>>>>>>>>>>>", this.notificationList);

          }


        }, error => {

          this.responsAlert();

        });

      } else {
        this.responsAlert();
      }

    });


  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');


    this.storage.get('mobile_user_id').then(mobileUserId => {
      console.log("MOBILE_USER_ID>>>>>>>>>>", mobileUserId);

      //Get Notifications
      this.getNotifications.getNotification(mobileUserId).subscribe(response => {
        console.log("NOTIFICATION>>>>>", response);
        let res = response.notificationList;
        if (res.responseCode == 1) {

          console.log("NOTIFICATIONS_RESPONSE>>>>>>>>>>", res.responseData);

          this.notificationList = res.responseData;
          console.log("ZZZZZZZZZZZ", this.notificationList);


          this.notificationListTemp.forEach(value => {

            this.notificationList.push(value);


          });




        }


      }, error => {

      });

    });

  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  openNotification(notifId: number) {
    this.navCtrl.push(ExpandNotification, {
      NotificationId: notifId
    });
  }


  responsAlert() {
    let alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Cannot connect to the server',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
          }
        }
      ]
    });
    alert.present();
  }

}
