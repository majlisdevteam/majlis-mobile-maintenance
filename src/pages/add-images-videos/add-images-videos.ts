import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MediaCapture, CaptureVideoOptions, CaptureError, MediaFile } from "@ionic-native/media-capture";
import { Transfer, TransferObject, FileUploadOptions } from "@ionic-native/transfer";
import { File } from '@ionic-native/file';
import { AppParams } from "../../app/app.module";
import { EventDetailsService } from "../../providers/event-details-service";
import { EventImages } from "../../models/event-images";
import { Storage } from '@ionic/storage';


//import { MediaCapture } from '@ionic-native/media-capture';

/**
 * Generated class for the AddImagesVideos page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-add-images-videos',
  templateUrl: 'add-images-videos.html',
  providers: [EventDetailsService,EventImages]
})
export class AddImagesVideos {

  @ViewChild('myvideo') myVideo: any;

  userId: number;
  eventId:number = 0;
  
  public photos: any;
  public base64Image: string;
  base64Image1
  constructor(public navCtrl: NavController,
    public navParams: NavParams, private camera: Camera,
    private alertCtrl: AlertController, private mediaCapture: MediaCapture,
    private transfer: Transfer, private file: File, public eventService: EventDetailsService,
    public storage: Storage,private eventImage : EventImages) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddImagesVideos');
    this.storage.get('mobile_user_id').then(mobileUserId => {
      console.log("MOBILE_USER_ID>>>>>>>>>>", mobileUserId);
      this.userId = mobileUserId;
    });

    this.eventId = this.navParams.get('EventID');

  }

  ngOnInit() {
    this.photos = [];
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.base64Image = 'data:image/jpeg;base64,' + imageData;

      // let eventImage = new EventImages();

      let arrImgData = Array<string>();

      arrImgData.push(this.base64Image);



      this.eventImage.imageData = arrImgData;

      this.eventImage.imageOwner  = this.userId;

      this.eventImage.imageEvent = this.eventId;

      console.log("----------------",this.eventImage);

      this.eventService.saveEventImages(this.eventImage).subscribe(res => {
        console.log("response ", res)
      }, error => {
        console.log("error ", error)
      })

      this.photos.push(this.base64Image);
      this.photos.reverse();
    }, (err) => {
      // Handle error
    });
  }

  deletePhoto(index) {

    let alert = this.alertCtrl.create({
      title: 'Delete selected photo?',
      message: '',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.photos.splice(index, 1);
          }
        }
      ]
    });
    alert.present();
  }

  /*accessGallery(){
     this.camera.getPicture({
       sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
       destinationType: this.camera.DestinationType.DATA_URL
      }).then((imageData) => {
        this.base64Image1 = 'data:image/jpeg;base64,'+imageData;
       }, (err) => {
        console.log(err);
      });
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad AddImagesVideos');
    }
  */




  startRecording() {
    let options: CaptureVideoOptions = { limit: 1, duration: 10, quality: 20 };
    this.mediaCapture.captureVideo(options)
      .then(((data: MediaFile[]) => {

        data.forEach((key, value) => {
          console.log("key", key);

          let options: FileUploadOptions = {
            fileKey: 'file',
            fileName : 'video.mp4',
            mimeType : 'image/mp4',
            chunkedMode : false,
            // params : {
            //   "abc" : "cde"
            // }
             headers :
               {"eventId" : this.eventId,"userId" : this.userId}

          }


          // console.log("value",value );
          const fileTransfer: TransferObject = this.transfer.create();

          let url = encodeURI(AppParams.VIDEO_SERVER_PATH + "events/saveEventVideo");

          console.log("encoded url ",url);

          fileTransfer.upload(key.fullPath, url, options,true)
            .then((data) => {
              // success
              console.log("########### upload success ##########");
            }, (err) => {
              // error
              console.log("########## upload error ###########", err
              );
            })

          //console.log(">>>>>>>>>>>>>>>>>>>>>");





        })


      }), ((error: CaptureError) => {

        console.log("###### Error #########")
      })

      // (data: MediaFile[]) => console.log(data),
      // (err: CaptureError) => console.error(err)
      )
    //console.log('Video Recorded. Result: ' + JSON.stringify(options));

  }




  // startRecording() {
  //   let options: CaptureVideoOptions = { limit: 1, duration: 10, quality: 20 };
  //   this.mediaCapture.captureVideo(options)
  //     .then(((data: MediaFile[]) => {

  //       data.forEach((key, value) => {
  //         console.log("key", key.fullPath);

  //         let videoOptions: FileUploadOptions = {
  //           fileKey: 'file',
  //           fileName: 'name.jpg',
  //           mimeType: "image/jpg",
  //           //  headers :
  //           //    {"Content-Type" : "multipart/form-data;boundary=558","Accept-Encoding": "multipart/form-data"}

  //         }


  //         // console.log("value",value );
  //         const fileTransfer: TransferObject = this.transfer.create();

  //         let url = AppParams.VIDEO_SERVER_PATH + "event/saveEventVideo";
  //         fileTransfer.upload(key.fullPath, url, videoOptions)
  //           .then((data) => {
  //             // success
  //             console.log("########### upload success ##########");
  //           }, (err) => {
  //             // error
  //             console.log("########## upload error ###########", err
  //             );
  //           })

  //         //console.log(">>>>>>>>>>>>>>>>>>>>>");





  //       })


  //     }), ((error: CaptureError) => {

  //       console.log("###### Error #########")
  //     })

  //     // (data: MediaFile[]) => console.log(data),
  //     // (err: CaptureError) => console.error(err)
  //     )
  //   //console.log('Video Recorded. Result: ' + JSON.stringify(options));

  // }



  selectVideo() {
    let video = this.myVideo.nativeElement;
    var options = {
      sourceType: 2,
      mediaType: 1,
      duration: 10
    };
    this.camera.getPicture(options).then((data) => {
      video.src = data;
      video.play();
    })
  }
}
