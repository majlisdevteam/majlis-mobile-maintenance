import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { CountryService } from "../../providers/country-service";
import { CountryModel } from "../../models/country";
import { ProvinceModel } from "../../models/province";
import { ProvinceDetailsModel } from "../../models/province-details";

/**
 * Generated class for the CountryList page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-province-list',
  templateUrl: 'province-list.html',
  providers: [CountryService]
})
export class ProvinceList {

  private tmpProvinceList: ProvinceModel[] = [];

  private provinceList: ProvinceModel[] = [];

  public countryCode;

  public provinceDetails  : ProvinceDetailsModel[] = [];

  constructor(public navCtrl: NavController, private loadingCtrl: LoadingController, public navParams: NavParams,
   public viewCtrl: ViewController, private countryService: CountryService, public toastCtrl  : ToastController) {

     this.countryCode= navParams.get("country");
  }

  ionViewDidLoad() {
    
    let me = this;

    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: 'ios'
      
    });
    loader.present();
    this.countryService.geProvinceList().subscribe(data => {

      console.log("data element ",data);

      this.provinceList = data;

      this.tmpProvinceList = this.provinceList;

      let pList = this.provinceList.filter(e=>{
        console.log("tmp list ",e)
          console.log("tmp country code ",this.countryCode)
        return e.countryCode == me.countryCode;
      });

      console.log("plist ",pList)
      if(pList.length !=0){

      this.provinceDetails= pList[0].provinces;

      }else{
        this.toastCtrl.create({
        message: 'Selected country does not support yet',
        duration: 3000,
      }).present();
      }


        console.log("province list ",this.provinceDetails);

      loader.dismiss();
    }, error => {
      loader.dismiss();
    });
  }

  cancelSelectProvince() {
    this.viewCtrl.dismiss();
  }

  onSelectProvince(obj){
    
    this.viewCtrl.dismiss(obj);
    
  }

  getProvinceItems(event) {

    let val = event.target.value;

    if (val && val.trim() != '') {

      this.tmpProvinceList = this.provinceList;

      this.tmpProvinceList = this.provinceList.filter((item) => {
        return (item.countryCode.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }else{
      this.tmpProvinceList = this.provinceList;
    }
  }

}
