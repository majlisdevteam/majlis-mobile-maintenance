import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { CountryService } from "../../providers/country-service";
import { CountryModel } from "../../models/country";
import { ProvinceModel } from "../../models/province";
import { ProvinceDetailsModel } from "../../models/province-details";

/**
 * Generated class for the CountryList page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-province-list',
  templateUrl: 'city-list.html',
  providers: [CountryService]
})
export class CityList {


  public provinceDetails  : ProvinceDetailsModel;

  constructor(public navCtrl: NavController, private loadingCtrl: LoadingController, public navParams: NavParams,
   public viewCtrl: ViewController, private countryService: CountryService) {
    
     this.provinceDetails= navParams.get("cities");

  }

  ionViewDidLoad() {
    
    let me = this;
    

    console.log("cities list ",this.provinceDetails.cities[0].cityName);
 
  }

  cancelSelectCountry() {
    this.viewCtrl.dismiss();
  }

  onSelectProvince(obj){
    
    this.viewCtrl.dismiss(obj);
    
  }

  getProvinceItems(event) {

    let val = event.target.value;

    if (val && val.trim() != '') {

      // this.tmpProvinceList = this.provinceList;

      // this.tmpProvinceList = this.provinceList.filter((item) => {
      //   return (item.countryCode.toLowerCase().indexOf(val.toLowerCase()) > -1);
      // });
    }else{
      // this.tmpProvinceList = this.provinceList;
    }
  }

}
