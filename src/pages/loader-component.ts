

import { LoadingController, Loading } from "ionic-angular";

export class LoaderComponent {

    public loadingCtrl: LoadingController;

    constructor(loadingCtrl: LoadingController){

        this.loadingCtrl = loadingCtrl;
    }    

    public getIosStyleLoader() : Loading {

       return this.loadingCtrl.create({
            content : 'Please wait...',
            spinner : 'ios'
        })



    }


    public getIosStyleLoaderVerification() : Loading {

       return this.loadingCtrl.create({
            content : 'Verifying...',
            spinner : 'ios'
        })



    }


       public getIosStyleLoaderCustom(text) : Loading {

       return this.loadingCtrl.create({
            content : text,
            spinner : 'ios'
        })



    }

}