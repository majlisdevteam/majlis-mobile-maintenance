import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppParams } from "../../app/app.module";
import { AbstractControl } from "@angular/forms/src/forms";
import { GroupEventModel } from "../../models/group-event";
import { EventDetailsService } from "../../providers/event-details-service";

/**
 * Generated class for the AboutUs page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'expand-event-video',
  templateUrl: 'expand-event-video.html'

})
export class ExpandEventVideo {


  videoFilePath: String;



  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Expand Video');

    this.videoFilePath = this.navParams.get('EventVideoID');

    console.log("AAAAAAAAAAAAAAAA",this.videoFilePath);

  }




}
