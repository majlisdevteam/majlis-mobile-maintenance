import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { CountryService } from "../../providers/country-service";
import { CountryModel } from "../../models/country";

/**
 * Generated class for the CountryList page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-country-list',
  templateUrl: 'country-list.html',
  providers: [CountryService]
})
export class CountryList {

  private tmpCountryList: CountryModel[] = [];

  private countryList: CountryModel[] = [];

  constructor(public navCtrl: NavController, private loadingCtrl: LoadingController, public navParams: NavParams,
   public viewCtrl: ViewController, private countryService: CountryService) {
  }

  ionViewDidLoad() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: 'ios'
      
    });
    loader.present();
    this.countryService.getCountryList().subscribe(data => {

      data.forEach(element => {

        let country = {
          name: element.name,
          countryCode: element.alpha2Code,
          flag: 'assets/country_flags/' + element.alpha2Code + '.png'
        }

        this.countryList.push(country);

      });

        this.tmpCountryList = this.countryList;

      loader.dismiss();
    }, error => {
      loader.dismiss();
    });
  }

  cancelSelectCountry() {
    this.viewCtrl.dismiss();
  }

  onSelectCountry(obj){
    
    this.viewCtrl.dismiss(obj);
    
  }

  getCountryItems(event) {

    let val = event.target.value;

    if (val && val.trim() != '') {

      this.tmpCountryList = this.countryList;

      this.tmpCountryList = this.tmpCountryList.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }else{
      this.tmpCountryList = this.countryList;
    }
  }

}
