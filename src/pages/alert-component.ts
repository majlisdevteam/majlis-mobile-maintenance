

import { AlertController, Alert } from "ionic-angular";

export class AlertComponent {

    public alertCtrl: AlertController;

    constructor(loadingCtrl: AlertController){

        this.alertCtrl = loadingCtrl;
    }    

    public getBasicAlert(titile,message) : Alert {

       return this.alertCtrl.create({
        title : titile,
        message : message
           
        })



    }


}