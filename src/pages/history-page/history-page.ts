import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLiteObject, SQLite } from "@ionic-native/sqlite";

/**
 * Generated class for the HistoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-history-page',
  templateUrl: 'history-page.html',
})
export class HistoryPage {
  histories = [];
  dbHistories = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,private sqlite: SQLite) {
    for (let i = 0; i < 30; i++) {
      this.histories.push(this.histories.length);
    }
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      for (let i = 0; i < 30; i++) {
        this.histories.push(this.histories.length);
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');

    this.LoadHistoryData();
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }




    public databseConnection() {
    return this.sqlite.create({
      name: "MajlisDB.db",
      location: "default"
    });


  }



   public LoadHistoryData() {
    this.databseConnection().then((database: SQLiteObject) => {
      database.executeSql("SELECT History_Type, History_Message, HDate FROM tbl_MajlisHistory", []).then((data) => {
  console.log(">>>>>>>>>>>>>>", data);
     
        for (let i = 0; i < data.rows.length; i++) {
          let history_Type = data.rows.item(i).History_Type;
          let history_Mesage = data.rows.item(i).History_Message;


          console.log("DB_HISTORY_TYPE>>>>>>>>>>> " + history_Type);
          console.log("DB_HISTORY_MESSAGE>>>>>>>>>>> " + history_Mesage);
          console.log("DB_HISTORY_DATE>>>>>>>>>>> " + history_Mesage);

          this.dbHistories.push({
            hType: data.rows.item(i).History_Type,
            hMessage: data.rows.item(i).History_Message,
            hDate: data.rows.item(i).Hdate
          });

        }
      });
    }, error => {

      console.log(">>>>>>>>>>>>>>>>>>>>>>>>database select error")
    });

  }

}
