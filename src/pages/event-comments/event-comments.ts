import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { EventDetailsService } from "../../providers/event-details-service";
import { MajlisEventComment } from "../../models/majlis-event-comment";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";


/**
 * Generated class for the EventComments page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-event-comments',
  templateUrl: 'event-comments.html',
  providers: [EventDetailsService, MajlisEventComment]
})
export class EventComments {

  public eventComment = new Array<MajlisEventComment>();
  public eventCommentTemp = new Array<MajlisEventComment>();

  eventId: number = 0;

  userPostComment: FormGroup;
  userComment: any = null;





  comments = [];
  eventTitle: String;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public getEventComments: EventDetailsService, public saveComment: MajlisEventComment,
    public formBuilder: FormBuilder, private alertCtrl: AlertController, private sqlite: SQLite) {

    this.userPostComment = formBuilder.group({
      userComment: ['', Validators.required],
    });

  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    //Get Event Comments
    this.eventId = this.navParams.get('EventID');
    console.log(">>>>>>>>>>>>>>EVENT_ID>>>>>>>>", this.eventId);

    this.eventTitle = this.navParams.get('eventTitle');


    //Get Event Comments
    this.getEventComments.getEventComment(this.eventId).subscribe(response => {
      console.log("EVENT_COMMENTS>>>>>", response);
      let res = response.getComments;
      if (res.responseCode == 1) {

        console.log("EVENT_WALL_RESPONSE_DATA", res.responseData);

        this.eventComment = res.responseData;
        console.log("HHHHHHHHHHHHHHHHHHH", this.eventComment);


      }
      infiniteScroll.complete();

    }, error => {
      infiniteScroll.complete();
    });




  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventComments');

    this.eventId = this.navParams.get('EventID');
    console.log(">>>>>>>>>>>>>>EVENT_ID>>>>>>>>", this.eventId);

    this.eventTitle = this.navParams.get('EventTitle');


    //Get Event Comments
    this.getEventComments.getEventComment(this.eventId).subscribe(response => {
      console.log("EVENT_COMMENTS>>>>>", response);
      let res = response.getComments;
      if (res.responseCode == 1) {

        console.log("EVENT_WALL_RESPONSE_DATA", res.responseData);

        this.eventComment = res.responseData;
        console.log("HHHHHHHHHHHHHHHHHHH", this.eventComment);


      }


    }, error => {

    });

  }


  // save Comment
  saveUserComment() {
    let iventCommentId;
    iventCommentId = this.navParams.get('EventID');
    this.saveComment.commentEventId = iventCommentId;
    this.getEventComments.saveEventComment(this.saveComment).subscribe(res => {



      console.log("success 2222>>>>>>>>>>> ", res);
      // let history_type;
      // let history_message;
      this.saveMobileUserHistory();

      this.ionViewDidLoad();

      this.userPostComment.reset();
      this.presentConfirm();

    },
      error => {
        console.log(">>>>>>>>>>>>>>>>> error in post comment ", error);
      })

  }



  // database connection
  public databseConnection() {
    return this.sqlite.create({
      name: "MajlisDB.db",
      location: "default"
    });


  }

  //save data into database
  saveMobileUserHistory() {
    console.log("Save Images clicked...................................");
    this.databseConnection().then((database: SQLiteObject) => {


      database.executeSql("INSERT INTO tbl_MajlisHistory (History_Type, History_Message) VALUES (?,?)", ['Comment', 'You have post a comment']).then((data) => {
        console.log("INSERTED COMMENT TO DATABASE SUCCESSFULLY .................. ", data);
      }, (error) => {
        console.log("ERROR INSERTION COMMENT TO DATABASE ..................", JSON.stringify(error.err));
      });

    });
  }





  presentConfirm() {
    let alert = this.alertCtrl.create({
      message: 'Your comment has been successfully added!',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
          }
        }
      ]
    });
    alert.present();
  }

}
