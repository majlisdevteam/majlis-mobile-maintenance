import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppParams } from "../../app/app.module";
import { AbstractControl } from "@angular/forms/src/forms";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


/**
 * Generated class for the AboutUs page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUs {

  private about: any

  myHtmlString: string;


  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
    this.myHtmlString = "<table><colgroup><col span='2' style='background-color:red'><col style='background-color:yellow'></colgroup><tr><th>ISBN</th><th>Title</th><th>Price</th></tr><tr><td>3476896</td><td>My first HTML</td><td>$53</td></tr></table>";

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutUs');
    this.about = AppParams.ABOUT;
  }

}
