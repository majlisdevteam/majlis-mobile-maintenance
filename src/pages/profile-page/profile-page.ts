import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { CountryList } from "../country-list/country-list";
import { MobileUserModel } from "../../models/mobile-users";
import { MobileUserService } from "../../providers/mobile-users-service";
import { EventCategoryModel } from "../../models/event-category";
import { EventCategoryService } from "../../providers/event-category-service";
import { EventWall } from "../event-wall/event-wall";
import { CountryService } from "../../providers/country-service";
import { GroupSelect } from "../group-select/group-select";
import { ProvinceDetailsModel } from "../../models/province-details";
import { ProvinceList } from "../province-list/province-list";
import { CityModel } from "../../models/city";
import { CityList } from "../city-list/city-list";
import { ProvinceModel } from "../../models/province";


/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile-page',
  templateUrl: 'profile-page.html',
  providers: [MobileUserModel, MobileUserService, EventCategoryService, CountryService, ProvinceDetailsModel, CityModel]
})
export class ProfilePage {

  private catList: Array<EventCategoryModel> = [];

  public provinceDetails: ProvinceDetailsModel;
  public provinceName = null;
  public cityDetail: CityModel;
  public cityName = null;

  userRegistrationForm: FormGroup;

   public invalidNumber : boolean = true;

  public alert;

  //forum data
  mobileNo: any;
  userName: string;
  emailAddress: string;
  dateOfBirth: any;
  country: string;
  province: string;
  city: string;
  category: any;

  majlisCategoryListTmp = [];

  countryFlag: string;
  countryCode: string = "AE";


  userId: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController,
    public storage: Storage, private alertCtrl: AlertController, public mobileUser: MobileUserModel,
    public mobileUserService: MobileUserService,public formBuilder: FormBuilder, public selectedCategory: EventCategoryService,
    public countryList: CountryService, private loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    // this.getProfileData();

       this.userRegistrationForm = formBuilder.group({
      mobileNo: [''],
      userName: [''],
      emailAddress: [''],
      dateOfBirth: [''],
      country: [''],
      province: [''],
      city: [''],
      category: ['']
    });
  }


  ionViewDidLoad() {

    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: 'ios'

    });
    loader.present();


    this.storage.get('mobile_user_id').then(mobileUserId => {

      this.userId = mobileUserId;



      //get category list
      this.selectedCategory.getAllCategory().subscribe(response => {

        let res = response.getCategoryDetailsMobile;

        if (res.responseCode == 1) {

          this.catList = res.responseData;
          console.log("category list ", this.catList)


          if (this.userId != 0) {

            //get user data
            this.mobileUserService.getMobileUsers(this.userId).subscribe(response => {
              let res = response.getMobileUserById.responseData;
              this.mobileUser = res;

              // this.mobileUser.mobileUserMobileNo = this.mobileUser.mobileUserMobileNo.split("+")[1];

              console.log("mobile users ",this.mobileUser);

              this.majlisCategoryListTmp = res.majlisCategoryList

              let arrCat = [];

              this.majlisCategoryListTmp.forEach(val => {
                arrCat.push(val.categoryId)
              });

              this.mobileUser.majlisCategoryList = arrCat;

              console.log("resss ", this.mobileUser)


              let cntryCode = this.mobileUser.mobileUserCountry;

              this.getCityListByProvince(cntryCode, this.mobileUser.mobileUserProvince);



              let arrCountry = [];

              this.countryList.getCountryList().subscribe(response => {
                arrCountry = response;

                let countryOf = arrCountry.filter(data => {

                  return data.alpha2Code == cntryCode;

                });

                this.country = countryOf[0].name;

                loader.dismiss();

              }, error => {
                this.responsAlert();
                loader.dismiss();
              })

            }, error => {
              loader.dismiss();
              this.responsAlert();
            });

          }

        } else {
          //error
          loader.dismiss();
          this.responsAlert();
        }

      }, error => {
        loader.dismiss();
        this.responsAlert();
      });

    })

  }

    mobileNumberBlur() {

    let mobileNo: string = this.mobileUser.mobileUserMobileNo;

    console.log("mobile ", mobileNo);

    let mobileMsg = "";

    if (mobileNo != null) {

      if (mobileNo.length == 0) {
        mobileMsg += "Mobile number should not be empty";
      } else if (mobileNo.indexOf("+") != -1) {
        mobileMsg += "Mobile number cannot have + sign twice ";
      } else if (mobileNo.length < 10 && mobileNo.length < 15) {
        mobileMsg += "Mobile number length is incorrect";
      } else {
        console.log("works well");
      }

      if (mobileMsg != "") {

        this.invalidNumber = false;

        this.toastCtrl.create({
          message: mobileMsg,
          duration: 2000,
          position: 'bottom'
        }).present();
      }else{
        this.invalidNumber = true;
      }

    }

    console.log("mobile msg ", mobileMsg);


  }



  getCityListByProvince(country, province): any {

    this.countryList.geProvinceList().subscribe(response => {
      let arrProvinceModel = new Array<ProvinceModel>();

      arrProvinceModel = response;

      let m = arrProvinceModel.filter(e => {

        return e.countryCode == country;
      })[0].provinces;

      console.log("KMM ", m)

      this.provinceDetails = m.filter(e => {
        return e.provinceName == province;
      })[0];

      console.log("this.provinceDetails ", this.provinceDetails)

    });

  }

  openCountryList($event) {
    let modalCountry = this.modalCtrl.create(CountryList);
    modalCountry.onDidDismiss(data => {
      console.log("data ", data);

      if (data != undefined) {

        let oldCountry = this.country;

        this.country = data.name;

        if (this.country != oldCountry) {
          this.mobileUser.mobileUserCity = null;
          this.mobileUser.mobileUserProvince = null;
        }

        this.countryFlag = data.flag;
        this.countryCode = data.countryCode;
        this.country = data.name;

        this.mobileUser.mobileUserCountry = this.countryCode;

      }
    });

    modalCountry.present()

  }

  openProvinceList($event) {

    let modalProvince = this.modalCtrl.create(ProvinceList, { country: this.countryCode });
    modalProvince.onDidDismiss(data => {
      console.log("data ", data);

      if (data != undefined) {

        this.provinceDetails = data;

        console.log("city details", this.provinceDetails)

        let oldProvince = this.provinceName;

        this.provinceName = data.provinceName;
        this.mobileUser.mobileUserProvince = data.provinceName;

        if (this.provinceName != oldProvince) {
          this.mobileUser.mobileUserCity = null;
        }

      }
    });

    modalProvince.present()

  }

  updateUser() {

    let loader = this.loadingCtrl.create({
      content: "Saving Changes...",
      spinner: 'ios'

    });
    loader.present();

    let me = this;

    console.log("mobile user ",this.mobileUser);

    this.mobileUserService.updateMobileUsers(this.mobileUser).subscribe(res => {


      if (res.modifyMobileUser.responseCode == 1) {
        this.presentConfirm(me);

      } else {
        this.toastCtrl.create({
          message: 'Cannot save your information at this time',
          duration: 2000,
          position: 'bottom'
        }).present();
      }

      loader.dismiss();

    },
      error => {
    loader.dismiss();
        this.toastCtrl.create({
          message: 'Please check your internet connection!',
          duration: 2000,
          position: 'bottom'
        }).present();
        console.log("success ", error);
      })



  }


  presentConfirm(me) {
    
    let alert = this.alertCtrl.create({
      message: 'Your profile has been successfully updated!',
      buttons: [
        {
          text : "Just fine",
          role : 'cancel',
          handler : ()=>{
            console.log("click just")

            me.navCtrl.setRoot(EventWall);
          }
        },
        {
          text: 'Change Group Selection',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
            this.navCtrl.push(GroupSelect, { mobileUserRegister: me.mobileUser, country: me.mobileUser.mobileUserCountry, editMode : true });
          }
        }
      ]
    });
    alert.present();
  }

  openCityList($event) {

    console.log("city list ", this.provinceDetails)

    if (this.provinceDetails != undefined) {

      let modalProvince = this.modalCtrl.create(CityList, { cities: this.provinceDetails });
      modalProvince.onDidDismiss(data => {
        console.log("data ", data);

        if (data != undefined) {

          this.cityDetail = data;

          console.log("city details", this.cityDetail)

          this.cityName = this.cityDetail.cityName;
          this.mobileUser.mobileUserCity = this.cityDetail.cityName;
          //this.countryName = data.name;

        }
      });

      modalProvince.present()

    } else {
      this.toastCtrl.create({
        message: 'Please select province first',
        duration: 2000,
        position: 'bottom'
      }).present();
    }

  }

  responsAlert() {
    this.alert.dismiss();
    this.alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Cannot connect to the server',
      buttons: [
        {
          text: 'Try Again',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
            this.ionViewDidLoad();
          }
        }
      ]
    });
    this.alert.present();
  }

}
