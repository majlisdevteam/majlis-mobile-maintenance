import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Toast, AlertController } from 'ionic-angular';
import { EventWall } from "../event-wall/event-wall";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { MobileUserModel } from "../../models/mobile-users";
import { MobileUserService } from "../../providers/mobile-users-service";
import { UserGroupService } from "../../providers/user-group-service";
import { MajlisGroupModel } from "../../models/majlis-group";
import { AppParams } from "../../app/app.module";
import { Storage } from '@ionic/storage';
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";
import { GroupDetails } from "../group-details/group-details";

/**
 * Generated class for the Terms page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'group-select',
  templateUrl: 'group-select.html',
  providers: [MobileUserModel, MobileUserService, UserGroupService]
})
export class GroupSelect {
  items = [];
  groupSelectionForm: FormGroup;
  groupNameCheck: string = null;
  start: number = 0;
  groupCountry;
  total: number;
  group: Array<MajlisGroupModel> = [];
  group2: boolean;
  private country: string;

  static toast: Toast;

  public editMode: boolean = false;


  private groupList = new Array<MajlisGroupModel>();
  private groupListTemp = new Array<MajlisGroupModel>();

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
    public mobileUser: MobileUserModel, public mobileUserService: MobileUserService,
    private loadingCtrl: LoadingController, private toastCtrl: ToastController,
    public showGroups: UserGroupService, public storage: Storage, private alertCtrl: AlertController,
    private sqlite: SQLite) {


    this.groupSelectionForm = formBuilder.group({
      groupNameCheck: ['', Validators.required],
      group2: ['']
    });
  }

  
  ionViewDidLoad() {

    let loader = this.loadingCtrl.create({
      content: "Loading Groups...",
      spinner: 'ios'
    });

    loader.present();


    if (this.navParams.get('user') != undefined) {
      GroupSelect.toast = this.toastCtrl.create({
        message: 'United Arab Emirates Selected as Country',
        duration: 2000,
        position: 'top'
      });
      GroupSelect.toast.present();
    }

    if (this.navParams.get("editMode") != undefined) {

      this.editMode = this.navParams.get("editMode");

    }

    this.country = this.navParams.get("country");
    this.mobileUser = this.navParams.get("mobileUserRegister");
    
    this.mobileUser.majlisGroupList.forEach(e=>{
      this.group.push(e);
    })

    this.showGroups.getUserGroup(this.start, this.country, this.mobileUser.mobileUserProvince, this.mobileUser.mobileUserCity).subscribe(response => {
      let res = response.getAllMobileGroups;

      this.groupList = res.responseData;
      console.log("RESPONSE_DATA", this.groupList);
      this.start = AppParams.LIMIT;
      this.total = res.totalRecords;


      loader.dismiss();

    }, error => {
      loader.dismiss();

      this.responsAlert();

    });



  }

  selectedGrp(groupId): boolean{
   
   return this.mobileUser.majlisGroupList.findIndex(e=>e.groupId == groupId) !=-1;
   
  }

  saveGroupChanges() {

    this.mobileUser.majlisGroupList = this.group;

       let loader = this.loadingCtrl.create({
      content: "Saving Changes...",
      spinner: 'ios'

    });
    loader.present();

    this.mobileUserService.updateMobileUsers(this.mobileUser).subscribe(response=>{
      console.log("log response ",response);

      let res = response.modifyMobileUser;

      loader.dismiss();

      if(res.responseCode==1){
        this.toastCtrl.create({
          message : 'Your changes successfully saved',
          duration : 3000
        }).present();


        this.navCtrl.setRoot(EventWall);
      }else{
           this.toastCtrl.create({
          message: 'Cannot save your information at this time',
          duration: 2000,
          position: 'bottom'
        }).present();
      
      }



    },error=>{
         this.toastCtrl.create({
          message: 'Cannot save your information at this time',
          duration: 2000,
          position: 'bottom'
        }).present();
      

    })


  }

  onTapGroup(groupId,groupName){

    console.log("tap group ic ",groupId)

    this.navCtrl.push(GroupDetails,{groupName: groupName, groupId: groupId});

  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    this.showGroups.getUserGroup(this.start, this.country, this.mobileUser.mobileUserProvince, this.mobileUser.mobileUserCity).subscribe(response => {
      let res = response.getAllGroupLevels;

      this.start = this.start + AppParams.LIMIT;



      this.groupListTemp = res.responseData;



      this.groupListTemp.forEach(value => {

        this.groupList.push(value);

      })


      infiniteScroll.complete();

    }, error => {
      // infiniteScroll.complete();
    });

  }



  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  // goToEventWall(IsSkip: boolean) {
  //   this.navCtrl.setRoot(EventWall);
  // }
  getSelectedGroup(groupId, $event) {


    console.log("groupId", groupId);

    let status = $event.checked;
    let mGroup = new MajlisGroupModel();

    console.log("count before ",this.group)

    if (status == true) {


      mGroup.groupId = groupId;
      this.group.push(mGroup);

    } else {


    let g=  this.group.findIndex(e=>e.groupId == groupId)

      console.log("g ",g);


      this.group.splice(g,1);
      
    }

    console.log("count after ",this.group)

  }



  finishUserReg() {

    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: 'ios'
    });

    loader.present();
    // this.getSelectedGroup();

    if (this.navParams.get('user') != undefined) {
      this.mobileUser = this.navParams.get('user');


    } else if (this.navParams.get('mobileUserRegister') != undefined) {
      this.mobileUser = this.navParams.get('mobileUserRegister');
    }

    this.mobileUser.majlisGroupList = this.group;
    this.mobileUserService.saveMobileUsers(this.mobileUser).subscribe(res => {


      console.log("success>>>>>>>>>>> ", res);
      if (res.mobileUserCreation.responseCode != undefined || res.mobileUserCreation.responseCode != null) {

        if (res.mobileUserCreation.responseCode != 999 || res.mobileUserCreation.responseCode != -1) {
          this.storage.set("mobile_user_id", res.mobileUserCreation.responseCode);
          // this.storage.set("saved-user-data", this.mobileUser);
          this.storage.set("mobile_user_mobile_no", this.mobileUser.mobileUserMobileNo);

        } else {
          this.responsAlert();
        }



        this.navCtrl.setRoot(EventWall);
        loader.dismiss();
      } else {
        loader.dismiss();
        this.responsAlert();
      }


    },
      error => {
        console.log("success ", error);
        this.responsAlert();
      })






  }

  responsAlert() {
    let alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Cannot connect to the server',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
          }
        }
      ]
    });
    alert.present();
  }



}