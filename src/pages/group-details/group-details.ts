import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AppParams } from "../../app/app.module";
import { AbstractControl } from "@angular/forms/src/forms";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MobileUserModel } from "../../models/mobile-users";
import { MobileUserService } from "../../providers/mobile-users-service";
import { LoaderComponent } from "../loader-component";
import { Storage } from '@ionic/storage';
import { EventWall } from "../event-wall/event-wall";
import { EventCategoryService } from "../../providers/event-category-service";
import { RegistrationPage } from "../registration/registration";
import { AlertComponent } from "../alert-component";
import { UserGroupService } from "../../providers/user-group-service";

/**
 * Generated class for the AboutUs page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'group-details',
  templateUrl: 'group-details.html',
  providers: [UserGroupService, ]
})
export class GroupDetails {


  public loadingCmp: LoaderComponent;

  public alertCmp: AlertComponent;

  public groupName;
  public groupId;

  public eventWall;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public toastCtrl: ToastController, public storage: Storage, public groupService: UserGroupService,
    public alertCtrl: AlertController, loadingCtrl: LoadingController) {

    this.loadingCmp = new LoaderComponent(loadingCtrl);
  }


  ionViewDidLoad() {

    let me = this;

    let loader = this.loadingCmp.getIosStyleLoaderCustom("Loading group details");

    loader.present();

    this.groupName = this.navParams.get("groupName");

    this.groupId = this.navParams.get("groupId");

    console.log("group id ",this.groupId);

    this.groupService.getEventsByGroup(this.groupId).subscribe(response=>{

      let res = response.getEventsByGroupMobile;

      console.log("respoi",res);

      if(res.responseCode==1){

        me.eventWall = res.responseData;

      }else{
        me.toastCtrl.create({
          message : "Cannot get events details",
          duration : 3000
        })
      }


      loader.dismiss();
    },error=>{
      loader.dismiss();
    });

  }


}
