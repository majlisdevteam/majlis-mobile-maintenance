import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ToastController, MenuController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { EventWall } from "../event-wall/event-wall";
import { CountryList } from "../country-list/country-list";
import { Storage } from '@ionic/storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { MobileUserModel } from "../../models/mobile-users";
import { MobileUserService } from "../../providers/mobile-users-service";
import { RequestOptionsArgs, RequestOptions, Headers } from "@angular/http";
import { EventCategoryModel } from "../../models/event-category";
import { MobileUsersEventCategoryModel } from "../../models/mobileusers-event-category";
import { GroupSelect } from "../group-select/group-select";
import { ProvinceList } from "../province-list/province-list";
import { ProvinceDetailsModel } from "../../models/province-details";
import { CityList } from "../city-list/city-list";
import { CityModel } from "../../models/city";



/**
 * Generated class for the Registration page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
  providers: [MobileUserModel, MobileUserService]
})
export class RegistrationPage {

  private catList: Array<EventCategoryModel> = [];

  userRegistrationForm: FormGroup;

  currentDate: number = new Date().getFullYear();


  countryName: string = "United Arab Emirates";
  countryFlag: string;
  countryCode: string = "AE";

  public provinceName = null;
  public provinceDetails: ProvinceDetailsModel;

  public cityDetail: CityModel;
  public cityName = null;

  //forum data
  mobileNo: any = null;
  userName: string = null;
  emailAddress: string = null;
  dateOfBirth: any = null;
  country: string = null;
  province: string = null;
  city: string = null;
  category: Array<EventCategoryModel> = [];

  arrYears = [];



  constructor(public navCtrl: NavController, public navParams: NavParams,
    private modalCtrl: ModalController, public storage: Storage,
    public formBuilder: FormBuilder, private sqlite: SQLite, public mobileUser: MobileUserModel,
    public mobileUserService: MobileUserService, public menu: MenuController, private loadingCtrl: LoadingController, public toastCtrl: ToastController) {

    this.menu.swipeEnable(false);

    this.userRegistrationForm = formBuilder.group({
      mobileNo: [''],
      userName: [''],
      emailAddress: [''],
      dateOfBirth: [''],
      country: [''],
      province: [''],
      city: [''],
      category: ['']
    });

    // this.mobileUser.mobileUserDob = "1960-01-01";;
  }

  mobileNumberBlur() {

    let mobileNo: string = this.mobileUser.mobileUserMobileNo;

    console.log("mobile ", mobileNo);

    let mobileMsg = "";

    if (mobileNo != null) {

      if (mobileNo.length == 0) {
        mobileMsg += "Mobile number should not be empty";
      } else if (mobileNo.indexOf("+") != -1) {
        mobileMsg += "Mobile number cannot have + sign twice ";
      } else if (mobileNo.length < 10 || mobileNo.length > 15) {
        mobileMsg += "Mobile number length is incorrect";
      } else {
        console.log("works well");
      }

      if (mobileMsg != "") {


        this.toastCtrl.create({
          message: mobileMsg,
          duration: 2000,
          position: 'bottom'
        }).present();
      }else{

      }

    }


  }


  ionViewDidLoad() {
   
    let year = new Date().getFullYear();
    
    for(let i=1960; i<=year; i++){
      this.arrYears.push(i);
    }


    this.catList = this.navParams.get("category");
    this.mobileUser.mobileUserMobileNo = this.navParams.get("userNumber");


  }


  openGroupSelectPage() {

    let mobileUser: MobileUserModel = new MobileUserModel();

    mobileUser.mobileUserMobileNo = this.mobileUser.mobileUserMobileNo;

    console.log("mobile user number ",mobileUser)


    this.navCtrl.push(GroupSelect, {
      user: mobileUser,
      country: "AE"
    });

    console.log("USER_MOBILE_NUMBER");
  }


  openCountryList($event) {
    let modalCountry = this.modalCtrl.create(CountryList);
    modalCountry.onDidDismiss(data => {
     
      const d = this.countryName

      if (data != undefined) {

        this.countryName = data.name;

        if(d !=data.name){

          this.provinceName = null;

          this.cityName = null;
        }

        this.countryFlag = data.flag;
        this.countryCode = data.countryCode;
        //this.countryName = data.name;

      }
    });

    modalCountry.present()

  }


  openProvinceList($event) {

    let modalProvince = this.modalCtrl.create(ProvinceList, { country: this.countryCode });
    modalProvince.onDidDismiss(data => {
     
      const d = this.provinceName

      if (data != undefined) {

        this.provinceDetails = data;

        console.log("city details", this.provinceDetails)

        this.provinceName = data.provinceName;
        
        if(d !=data.name){
          
          this.cityName = null;
        }

      }
    });

    modalProvince.present()

  }


  openCityList($event) {

    console.log("city list ", this.provinceDetails)

    if (this.provinceDetails != undefined) {

      let modalProvince = this.modalCtrl.create(CityList, { cities: this.provinceDetails });
      modalProvince.onDidDismiss(data => {
        console.log("data ", data);

        if (data != undefined) {

          this.cityDetail = data;

          console.log("city details", this.cityDetail)

          this.cityName = data.cityName;
          //this.countryName = data.name;

        }
      });

      modalProvince.present()

    } else {
      this.toastCtrl.create({
        message: 'Please select province first!',
        duration: 2000,
        position: 'bottom'
      }).present();
    }

  }


  registerUser() {

    this.mobileUser.mobileUserCountry = this.countryCode;
    this.mobileUser.mobileUserCity = this.cityName;
    this.mobileUser.mobileUserProvince = this.provinceName;
    this.mobileUser.mobileUserMobileNo = this.mobileUser.mobileUserMobileNo
    this.category = this.mobileUser.majlisCategoryList;

    let arrCat = new Array<EventCategoryModel>();
    this.category.forEach((val, index) => {

      let catM = new EventCategoryModel;

      catM.categoryId = val;

      arrCat.push(catM);

    });

    this.mobileUser.majlisCategoryList = arrCat;

    console.log("event cart ", this.mobileUser);

    console.log("GVHHJGVFGJGJJFCJGCFHC", this.mobileUser);
    this.navCtrl.push(GroupSelect, { mobileUserRegister: this.mobileUser, country: this.mobileUser.mobileUserCountry });

  }

}
