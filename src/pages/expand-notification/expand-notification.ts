import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NotificationService } from "../../providers/notification-service";
import { NotificationModel } from "../../models/majlis-notification";
import { Storage } from '@ionic/storage';


/**
 * Generated class for the ExpandNotification page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-expand-notification',
  templateUrl: 'expand-notification.html',
  providers: [NotificationService]
})
export class ExpandNotification {
  
  private notificationList: Array<NotificationModel> = [];


  notifId: number = 0;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public getNotificationDetails: NotificationService, public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExpandNotification');


    this.notifId = this.navParams.get('NotificationId');
    console.log(">>>>>>>>>>>>>>NOTIF_ID>>>>>>>>", this.notifId);

    //Get Notifications Details
    this.getNotificationDetails.getNotificationDetails(this.notifId).subscribe(response => {
      console.log("NOTIFICATION_DETAILS>>>>>>>>>", response);
      let res = response.notificationList;
      if (res.responseCode == 1) {

        console.log("NOTIFICATIONS_DETAILS_RESPONSE>>>>>>>>>>", res.responseData);

        this.notificationList = res.responseData;
        console.log("NOTIFICATIONS_DETAILS_RESPONSE_DATA>>>>>>>>>>>>>", this.notificationList);


      }


    }, error => {

    });

  }

}
