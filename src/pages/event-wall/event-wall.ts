import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Select } from 'ionic-angular';
import { Notifications } from "../notifications/notifications";
import { NearbyEvents } from "../nearby-events/nearby-events";
import { EventComments } from "../event-comments/event-comments";
import { EventDetailsService } from "../../providers/event-details-service";
import { GroupEventModel } from "../../models/group-event";
import { Start } from "../start/start";
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";
import { RSVPStatusModel } from "../../models/rsvp-status";
import { Storage } from '@ionic/storage';
import { RegistrationPage } from "../registration/registration";
import { AppParams } from "../../app/app.module";

/**
 * Generated class for the EventWall page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-event-wall',
  templateUrl: 'event-wall.html',
  providers: [EventDetailsService, GroupEventModel]
})
export class EventWall {

  imageNotFound: String = "Not Found";
  public eventWall = new Array<GroupEventModel>();
  public eventWallTemp = new Array<GroupEventModel>();

  private eventDetailsStatus: Array<GroupEventModel> = [];
  private eventDetailsStatusTemp: Array<GroupEventModel> = [];

  public eventRsvpStatus;

  public eventRsvpId;

  private userRSVPStatus = [];
  private userRSVPStatusTemp: Array<GroupEventModel> = [];

  public eventId: any;

  public limitStart = AppParams.START;
  public limitStop = AppParams.LIMIT;

  public userId: any;
  public userMobileNumber: any;

  public userRSVP: any;

  public loading   = {};

  public changeStatus = false;

  items = [];
  eventTitle: String = "Live Music Night";

  @ViewChild('selecteRSVP') selecteRSVP: Select;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private alertCtrl: AlertController, public getEventWallDetails: EventDetailsService,
    private loadingCtrl: LoadingController, public groupEvent: GroupEventModel, public storage: Storage,
    private sqlite: SQLite) {

  }

 

  getSearchedEvent($event) {
    console.log("anit", $event.target.value)

    let search = $event.target.value;

    if (search == "" || search == null) {
      this.storage.set("searchKey", "");
    } else {
      this.storage.set("searchKey", search);
    }

    this.getEventWallDetails.getEventDetailsSearch(search).subscribe(response => {
      console.log("EVENT_WALL>>>>>", response);
      let res = response.getUserEvents;
      if (res.responseCode == 1) {

        this.eventWall = res.responseData;

        this.limitStart = this.limitStart + this.limitStop;

        this.eventWall.forEach(value => {


          this.eventWallTemp.push(value);
          console.log("abcd ", value)
          this.userRSVP = value.eventStatus;

        })


        this.getEventWallDetails.getEventRSVPStatus(this.eventId, this.userId).subscribe(response => {
          console.log("EVENT_RSVP STATUS>>>>>", response);
          let res = response.getEventStatusUser;
          if (res.responseCode == 1) {

            console.log("EVENT_RSVP STATUS_DATA", res.responseData);

            this.userRSVPStatus = res.responseData;
            console.log("STATUS_RSVP RESPONSE_DATA>>>>", this.userRSVPStatus);

            this.userRSVPStatusTemp.forEach(value => {

              this.userRSVPStatus.push(value);

            })
          }
        }, error => {

        });


      } else {
        this.responsAlert();
      }

    }, error => {

    });


  }

  eventStatus(eventId, statusId) {

    console.log(">>>> ", statusId);

    this.eventRsvpId = statusId;

    this.eventId = eventId;
    this.selecteRSVP.open();
  }





  ionViewDidLoad() {

    this.storage.get('mobile_user_mobile_no').then(mobileNumber => {
      this.userMobileNumber = mobileNumber;
    });

    this.storage.get('mobile_user_id').then(mobileUserId => {
      console.log("MOBILE_USER_ID>>>>>>>>>>", mobileUserId);
      this.userId = mobileUserId;


      console.log('ionViewDidLoad EventWall');

      let loader = this.loadingCtrl.create({
        content: "Loading",
        spinner: 'ios'
      });
      loader.present();

      //Get Event Details
      this.getEventWallDetails.getEventWall(this.userId, this.limitStart, this.limitStop).subscribe(response => {

        let res = response.getUserEvents;
        if (res.responseCode == 1) {


          this.eventWall = res.responseData;

          this.limitStart = this.limitStart + this.limitStop;

          this.eventWall.forEach(value => {

            this.loading[value.eventId] = false;

            this.eventWallTemp.push(value);
            this.userRSVP = value.eventStatus;

          })


          this.getEventWallDetails.getEventRSVPStatus(this.eventId, this.userId).subscribe(response => {
            console.log("EVENT_RSVP STATUS>>>>>", response);
            let res = response.getEventStatusUser;
            if (res.responseCode == 1) {

              console.log("EVENT_RSVP STATUS_DATA", res.responseData);

              this.userRSVPStatus = res.responseData;
              console.log("STATUS_RSVP RESPONSE_DATA>>>>", this.userRSVPStatus);

              this.userRSVPStatusTemp.forEach(value => {

                this.userRSVPStatus.push(value);

              })
            }
          }, error => {

          });


        } else {
          this.responsAlert();
        }

        loader.dismiss();
      }, error => {

        loader.dismiss();
        this.responsAlert();

      });



      this.getEventWallDetails.getEventStatus().subscribe(response => {
        console.log("EVENT_STATUS>>>>>", response);
        let res = response.getEventStatus;
        if (res.responseCode == 1) {

          console.log("EVENT_STATUS_DATA", res.responseData);

          this.eventDetailsStatus = res.responseData;
          console.log("STATUS_RESPONSE_DATA>>>>", this.eventDetailsStatus);

          this.eventDetailsStatusTemp.forEach(value => {

            this.eventDetailsStatus.push(value);

          })



        }


      }, error => {

      });

    });



  }

  getRsvpByStatusId(id) {


    return this.userRSVPStatus.filter(e => {
      return e.statusId === id;
    })[0];


  }

  selectRsvp(value) {

    console.log("rsvp..... ", value);

    let rsvpStatus: RSVPStatusModel = new RSVPStatusModel();

    rsvpStatus.eventId = this.eventId
    rsvpStatus.statusId = value;
    rsvpStatus.userId = this.userId;

    let eventIndex = this.eventWall.findIndex(e => e.eventId == this.eventId);

    let statusObj = [
      {
        eventId: null,
        status: this.getRsvpByStatusId(value).status,
        statusId: value
      }
    ]



    this.changeStatus = true

    this.getEventWallDetails.saveEventStatus(rsvpStatus).subscribe(res => {
      console.log("res ", eventIndex);

      console.log("before ", this.eventWall[eventIndex])

      this.eventWall[eventIndex].majlisGroupEventStatusList = statusObj;

      console.log("after ", this.eventWall[eventIndex])

      this.changeStatus = false;

      // this.saveMobileUserHistory();

    }, error => {

    })

  }


  // save event RSVP to local database
  // database connection
  public databseConnection() {
    return this.sqlite.create({
      name: "MajlisDB.db",
      location: "default"
    });


  }

  //save data into database
  saveMobileUserHistory() {
    console.log("Save Images clicked...................................");
    this.databseConnection().then((database: SQLiteObject) => {


      database.executeSql("INSERT INTO tbl_MajlisHistory (History_Type, History_Message) VALUES (?,?)", ['Event', 'You have added RSVP']).then((data) => {
        console.log("INSERTED COMMENT TO DATABASE SUCCESSFULLY .................. ", data);
      }, (error) => {
        console.log("ERROR INSERTION COMMENT TO DATABASE ..................", JSON.stringify(error.err));
      });

    });
  }



  doInfinite(infiniteScroll) {


    this.storage.get('mobile_user_id').then(mobileUserId => {
      console.log("MOBILE_USER_ID>>>>>>>>>>", mobileUserId);
      this.userId = mobileUserId;


      console.log('Begin async operation');


      //Get Event Details
      this.getEventWallDetails.getEventWall(this.userId, this.limitStart, this.limitStop).subscribe(response => {

        console.log("EVENT_WALL>>>>>", response);

        let res = response.getUserEvents;

        if (res.responseCode == 1) {

          console.log("EVENT_WALL_RESPONSE_DATA", res.responseData);

          this.eventWallTemp = res.responseData;


          if (this.eventWallTemp.length != 0) {



            this.eventWallTemp.forEach(value => {

              this.eventWall.push(value);


            });

            // Get event status
            this.getEventWallDetails.getEventStatus().subscribe(response => {
              console.log("EVENT_STATUS>>>>>", response);
              let res = response.getEventStatus;
              if (res.responseCode == 1) {

                console.log("EVENT_STATUS_DATA", res.responseData);

                this.eventDetailsStatus = res.responseData;
                console.log("STATUS_RESPONSE_DATA>>>>", this.eventDetailsStatus);

                this.limitStart = this.limitStart + this.limitStop;

                this.eventDetailsStatusTemp.forEach(value => {


                  // this.eventStatusOption.


                  this.eventDetailsStatus.push(value);

                })



              }
              infiniteScroll.complete();

            }, error => {
              infiniteScroll.complete();
            });



            console.log("limit start ", this.limitStart);

            // this.limitStop = this.limitStart + this.limitStop;

            console.log("limit stop ", this.limitStop);

          } else {


            console.log("no event are found");

            console.log("limit else start ", this.limitStart);

            console.log("limit else stop ", this.limitStop);

          }

          infiniteScroll.complete();

        } else {
          infiniteScroll.complete();
          this.responsAlert();
        }


      }, error => {
        infiniteScroll.complete();

        this.responsAlert();

      });



    });


  }

  openNotificationsPage() {
    let loader = this.loadingCtrl.create({
      content: "Loading",
    });
    loader.present();
    this.navCtrl.push(Notifications);
    loader.dismiss();
  }

  openSelectedEventPage(eventId: number, eventTitle, eventLoc, eventMsg, eventDate, eventTime, status) {

    this.eventRsvpStatus = status;

    this.storage.get('mobile_user_mobile_no').then(userMobileNo => {

      if (this.userId != undefined && userMobileNo != undefined) {

        this.navCtrl.push(NearbyEvents, {
          EventID: eventId,
          EventTitle: eventTitle,
          EventLoc: eventLoc,
          EventMsg: eventMsg,
          EventDate: eventDate,
          EventTime: eventTime,
          RsvpStatus: status,
          eventObj : this.eventWall
        });

      } else {
        this.notRegisteredAlert();
      }

    });

  }

  openEventCommentsPage(eventId: number) {

    this.navCtrl.push(EventComments, {
      EventID: eventId
    });
  }

  eventStatusAlert() {
    let alert = this.alertCtrl.create({
      title: 'Status',
      inputs: [
        {
          type: 'radio',
          label: 'Not Attending',
          value: 'notattend',
          checked: true
        },
        {
          type: 'radio',
          label: 'Attending',
          value: 'attend'
        },
        {
          type: 'radio',
          label: 'Maybe',
          value: 'maybe'
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Ok',
          handler: (data: any) => {
            console.log('Radio data:', data);
          }
        }
      ]
    });
    alert.present();
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  responsAlert() {
    let alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Cannot connect to the server',
      buttons: [
        {
          text: 'Try Again',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
            this.ionViewDidLoad();
          }
        }
      ]
    });
    alert.present();
  }

  notRegisteredAlert() {
    let alert = this.alertCtrl.create({
      title: 'Warning',
      message: 'Please Register!',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
            this.navCtrl.push(RegistrationPage);
          }
        }
      ]
    });
    alert.present();
  }


}
