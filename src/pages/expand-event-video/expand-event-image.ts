import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppParams } from "../../app/app.module";
import { AbstractControl } from "@angular/forms/src/forms";
import { GroupEventModel } from "../../models/group-event";
import { EventDetailsService } from "../../providers/event-details-service";

/**
 * Generated class for the AboutUs page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'expand-event-image',
  templateUrl: 'expand-event-image.html',

})
export class ExpandEventImage {

  imageIconPath: String;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Expand Image Page');

    this.imageIconPath = this.navParams.get('EventImageID');

    console.log("AAAAAAAAAAAAAAAA",this.imageIconPath);

  }


}
