import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, MenuController } from 'ionic-angular';
import { TermsPage } from "../terms/terms";
import { DisclaimerPage } from "../disclaimer/disclaimer";
import { RegistrationPage } from "../registration/registration";
import { EventCategoryService } from "../../providers/event-category-service";
import { EventCategoryModel } from "../../models/event-category";
import { Storage } from '@ionic/storage';
import { EventWall } from "../event-wall/event-wall";
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";
import { MobileNumber } from "../mobile-number/mobile-number";



/**
 * Generated class for the Start page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
  providers: [EventCategoryService]
})
export class Start {

  public userId: any

  private catList: Array<EventCategoryModel> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public allCategory: EventCategoryService, private alertCtrl: AlertController,
    private loadingCtrl: LoadingController, public storage: Storage, private sqlite: SQLite,
    public menuCtrl: MenuController) {

    this.menuCtrl.swipeEnable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Start');

    this.storage.get('mobile_user_id').then(mobileUserId => {
      console.log("MOBILE_USER_ID>>>>>>>>>>", mobileUserId);
      this.userId = mobileUserId;
    });



    this.sqlite.create({
      name: "MajlisDB.db",
      location: "default"
    }).then((database: SQLiteObject) => {
      database.executeSql("CREATE TABLE IF NOT EXISTS tbl_MajlisHistory (Id INTEGER PRIMARY KEY AUTOINCREMENT, History_Type VARCHAR(200), History_Message VARCHAR(500),HDate datetime default current_timestamp)", {}).then((data) => {
        console.log("TABLE MajlisUser CREATED: ", data);
        //this.refresh();
      }, (error) => {
        console.error("Unable to execute sql MajlisDB", error);
      });
    }, (error) => {
      console.error("Unable to open database", error);
    });



  }

  openTerms() {
    this.navCtrl.push(TermsPage);
  }

  opendisclaimer() {
    this.navCtrl.push(DisclaimerPage);
  }

  acceptTerms() {

    // loading mask
    // let loader = this.loadingCtrl.create({
    //   content: "Please wait...",
    //   spinner: 'ios'
    // });

    // loader.present();

    console.log("accept terms ",this.userId);

    


    // this.allCategory.getAllCategory().subscribe(response => {


    //   let res = response.categoryList;

    //   if (res.responseCode == 1) {

    //     this.catList = res.responseData;

    //     console.log("cat list ", this.catList)

    //     loader.dismiss();


    //     // ######################check if user already registered######################
        if (this.userId != undefined) {

          this.navCtrl.setRoot(EventWall);

        } else {
          // this.navCtrl.setRoot(MobileNumber, { category: this.catList });
          this.navCtrl.push(MobileNumber);
        }



    //   } else {
    //     loader.dismiss();
    //     //error
    //     console.log("Error in else...");
    //     this.responsAlert();
    //   }
    // }, error => {
    //   console.log("Error in connection...")
    //   loader.dismiss();
    //   this.responsAlert();
    // });


    // dissmiss loading mask


  }



  responsAlert() {
    let alert = this.alertCtrl.create({
      title: 'Information',
      message: 'Cannot connect to the server',
      buttons: [
        {
          text: 'Dismiss',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');
          }
        }
      ]
    });
    alert.present();
  }


  notRegisteredAlert() {
    let alert = this.alertCtrl.create({
      title: 'Warning',
      message: 'Please Register!',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            console.log('OK clicked');

          }
        }
      ]
    });
    alert.present();
  }

}
