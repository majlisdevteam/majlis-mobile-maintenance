import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AppParams } from "../../app/app.module";
import { AbstractControl } from "@angular/forms/src/forms";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MobileUserModel } from "../../models/mobile-users";
import { MobileUserService } from "../../providers/mobile-users-service";
import { LoaderComponent } from "../loader-component";
import { Storage } from '@ionic/storage';
import { EventWall } from "../event-wall/event-wall";
import { EventCategoryService } from "../../providers/event-category-service";
import { RegistrationPage } from "../registration/registration";
import { AlertComponent } from "../alert-component";

/**
 * Generated class for the AboutUs page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'mobile-number',
  templateUrl: 'mobile-number.html',
  providers: [MobileUserService,EventCategoryService]
})
export class MobileNumber {

  public invalidNumber: boolean = false;
  public mobileUser: MobileUserModel = new MobileUserModel();

  public loadingCmp: LoaderComponent;

  public alertCmp: AlertComponent;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public toastCtrl: ToastController, public storage: Storage, public userService: MobileUserService,
    public alertCtrl: AlertController, loadingCtrl: LoadingController, public allCategory: EventCategoryService) {

    this.loadingCmp = new LoaderComponent(loadingCtrl);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutUs');

  }

  verifyMobileNo() {

    let mobileNo: string = this.mobileUser.mobileUserMobileNo;

    console.log("mobile ", mobileNo);

    let mobileMsg = "";

    if (mobileNo != null) {

      if (mobileNo.length == 0) {
        mobileMsg += "Mobile number should not be empty";
      } else if (mobileNo.indexOf("+") != -1) {
        mobileMsg += "Mobile number cannot have + sign twice ";
      } else if (mobileNo.length < 10 || mobileNo.length > 15) {
        mobileMsg += "Mobile number length is incorrect";
      } else {
        console.log("works well");
      }

      if (mobileMsg != "") {

        this.invalidNumber = false;

        this.toastCtrl.create({
          message: "You need to enter 10 to 15 digit number with leading + sign",
          duration: 2000,
          position: 'bottom'
        }).present();
      } else {
        this.invalidNumber = true;

        let loader = this.loadingCmp.getIosStyleLoader();

        loader.present();

        this.userService.requestVerification(mobileNo).subscribe(response => {

          console.log(">>>> ", response.verifyMobileUserNumber.responseCode);

          loader.dismiss();

          if (response.verifyMobileUserNumber.responseCode == 1) {

            this.showPrompt(response.verifyMobileUserNumber.responseData);

          } else {


            let alert = this.alertCmp.getBasicAlert("Connection Error", "Cannot connect with the server!");

            alert.present();

          }

        }, error => {


          let alert = this.alertCmp.getBasicAlert("Connection Error", "Cannot connect with the server!");

          alert.present();

        })



      }

    }

    console.log("mobile msg ", mobileMsg);


  }


  showPrompt(value) {

    var me = this;

    let prompt = this.alertCtrl.create({
      title: 'Verification',
      message: "Please enter the verification code",
      inputs: [
        {
          name: 'Verification Code',
          placeholder: 'Verification code',
          id: 'cerificationCode',
          value: value
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Verify!',
          handler: data => {

            console.log("data ", data['Verification Code'])

            let verification = data['Verification Code'];

            let loader = me.loadingCmp.getIosStyleLoaderVerification();

            loader.present();

            me.userService.requestFinalVerification(this.mobileUser.mobileUserMobileNo, verification).subscribe(response => {

              console.log("response ", response);

              let res = response.finalVerification;

              loader.dismiss();

              if (res.responseCode == 1) {

                this.mobileUser = res.responseData

                console.log("response mobile ", this.mobileUser);

                this.storage.set("mobile_user_id", this.mobileUser.mobileUserId);
                this.storage.set("mobile_user_mobile_no", this.mobileUser.mobileUserMobileNo);

                me.navCtrl.setRoot(EventWall);

              } else if (res.responseCode == 2) {

                let loader = me.loadingCmp.getIosStyleLoaderCustom("Ok.. Please Wait..")

                loader.present();

                this.allCategory.getAllCategory().subscribe(response => {

                  let res = response.getCategoryDetailsMobile;

                  loader.dismiss();

                  if (res.responseCode == 1) {

                    let catList = res.responseData;

                    console.log("cat list ", catList)



                    this.navCtrl.setRoot(RegistrationPage, { category: catList, userNumber : me.mobileUser.mobileUserMobileNo });

                  } else {
                    loader.dismiss();
                  this.toastCtrl.create({
                    message : 'Verification code is invalid',
                    duration : 3000
                  }).present();
                    console.log("Error in else...");


                  }

                });

                // this.allCategory.getAllCategory().subscribe(response => {


                //   let res = response.categoryList;

                //   if (res.responseCode == 1) {

                //     this.catList = res.responseData;

                //     console.log("cat list ", this.catList)

                //     loader.dismiss();


                //     // ######################check if user already registered######################
                //     if (this.userId != undefined) {

                //       this.navCtrl.setRoot(EventWall);

                //     } else {
                //       this.navCtrl.setRoot(RegistrationPage, { category: this.catList });
                //     }



                //   } else {
                //     loader.dismiss();
                //     //error
                //     console.log("Error in else...");
                //     this.responsAlert();
                //   }
                // }, error => {
                //   console.log("Error in connection...")
                //   loader.dismiss();
                //   this.responsAlert();
                // });

              }else{
                 this.toastCtrl.create({
                    message : 'Verification code is invalid',
                    duration : 3000
                  }).present();
              }


            }, error => {

            });

          }
        }
      ]
    });
    prompt.present();
  }


}
