import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EventFeedbackModel } from "../../models/event-feedback";
import { EventDetailsService } from "../../providers/event-details-service";

/**
 * Generated class for the Feedback page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
  providers: [EventDetailsService]

})
export class Feedback {


  feedbackValue: number = 0;
  eventId: number;
  userId: number;
  feedbackCnt :any;




  constructor(public navCtrl: NavController, public navParams: NavParams, public getEventWallDetails: EventDetailsService, ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Feedback');
  }

  savefeedback(rating) {

console.log("feed>>>>>>>>>>>>>", this.feedbackCnt)
    this.eventId = this.navParams.get('EventID');
    this.userId = this.navParams.get('UserID');

    // console.log("feedback value :", this.feedbackValue);

    let eventFeedback: EventFeedbackModel = new EventFeedbackModel();
    eventFeedback.eventId = this.eventId;
    eventFeedback.userId = this.userId;
    eventFeedback.feedbackCnt = this.feedbackCnt;

    this.getEventWallDetails.saveEventFeedback(eventFeedback).subscribe(res => {
      console.log("res ", res);

      this.navCtrl.pop(Feedback);

    }, error => {
      console.log("error", error);

    })



  }



}
